#include <Arduino.h>
#include <Wire.h>
#include <avr/sleep.h>
#include "InputDebounce.h"

#define BUTTON_DEBOUNCE_DELAY 20

#define FIRMWARE_VERSION 1
#define READ_BUTTONS 2
#define LOW_POWER 8

static InputDebounce buttonUp;
static InputDebounce buttonRight;
static InputDebounce buttonDown;
static InputDebounce buttonLeft;
static InputDebounce buttonCenter;

static const pin_size_t kButtonUp = PIN_PA6;
static const pin_size_t kButtonRight = PIN_PA5;
static const pin_size_t kButtonDown = PIN_PA4;
static const pin_size_t kButtonLeft = PIN_PA7;
static const pin_size_t kButtonCenter = PIN_PA3;

static const pin_size_t kI2cScl = PIN_PB0;
static const pin_size_t kI2cSda = PIN_PB1;
static const pin_size_t kI2cInt = PIN_PB2;

static const uint8_t kVersion = 1;

static volatile uint8_t sCommand = 0;

static volatile bool sInterruptHandled = true;

void i2cReceive(int numBytes)
{
  while (Wire.available())
  {
    sCommand = Wire.read();
  }
}

void i2cRequest()
{
  if (sCommand == FIRMWARE_VERSION)
  {
    Wire.write(kVersion);
  }
  else if (sCommand == READ_BUTTONS)
  {

    uint8_t state = 0;
    state |= buttonUp.isPressed();
    state |= buttonRight.isPressed() << 1;
    state |= buttonDown.isPressed() << 2;
    state |= buttonLeft.isPressed() << 3;
    state |= buttonCenter.isPressed() << 4;
    Wire.write(state);

    sInterruptHandled = true;
    digitalWrite(kI2cInt, HIGH);
  }
}

void button_pressedCallback(uint8_t pinIn)
{
  sInterruptHandled = false;
  digitalWrite(kI2cInt, LOW);
}

void button_releasedCallback(uint8_t pinIn)
{
  sInterruptHandled = false;
  digitalWrite(kI2cInt, LOW);
}

void setup()
{
  // Set interrupt pin to output
  pinMode(kI2cInt, OUTPUT);
  digitalWrite(kI2cInt, HIGH);

  buttonUp.setup(kButtonUp, BUTTON_DEBOUNCE_DELAY);
  buttonUp.registerCallbacks(button_pressedCallback, button_releasedCallback);

  buttonRight.setup(kButtonRight, BUTTON_DEBOUNCE_DELAY);
  buttonRight.registerCallbacks(button_pressedCallback, button_releasedCallback);

  buttonDown.setup(kButtonDown, BUTTON_DEBOUNCE_DELAY);
  buttonDown.registerCallbacks(button_pressedCallback, button_releasedCallback);

  buttonLeft.setup(kButtonLeft, BUTTON_DEBOUNCE_DELAY);
  buttonLeft.registerCallbacks(button_pressedCallback, button_releasedCallback);

  buttonCenter.setup(kButtonCenter, BUTTON_DEBOUNCE_DELAY);
  buttonCenter.registerCallbacks(button_pressedCallback, button_releasedCallback);

  // Setup i2c callbacks
  Wire.pins(kI2cSda, kI2cScl);
  Wire.onReceive(i2cReceive);
  Wire.onRequest(i2cRequest);
  Wire.begin(0x1C); // Same address as qtouch controller

  set_sleep_mode(SLEEP_MODE_STANDBY);

  // 2 series AVR high power consumption of analog circuit
  ADC0.CTRLA &= ~ADC_ENABLE_bm;
}

void loop()
{
  buttonUp.process(millis());
  buttonRight.process(millis());
  buttonDown.process(millis());
  buttonLeft.process(millis());
  buttonCenter.process(millis());

  // handle lower power command?
  if (sCommand == LOW_POWER)
  {
    sCommand = 0;
    sleep_enable();
    sleep_cpu();
    sleep_disable();
  }
}
