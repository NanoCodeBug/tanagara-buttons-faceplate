# Button Faceplate for Tangara

Touch controls hate my fingers so I made a physical button faceplate. 

Based off of the reference design for a clicky faceplate

https://git.sr.ht/~jacqueline/tangara-clicky-faceplate

<img src="images/image2.png" style="width:200px;"/>

<img src="images/image1.png" style="width:200px;"/>


## Fabricate a faceplate
I used OSHPark and Digikey. Kicad files in the `tanagara-buttons-faceplate` folder.

### Part List:
| Count | Part | MPN |
|-------|------|-----|
| 5 | smd buttons | SKRRAAE010 ALPS
| 1 | ATTINY 826 | ATTINY826-SUR
| 1 | 1 uF 805 capacitor | GRM216R61C105KA88D
| 1 | Molex 15 pin connector | 505278-1533 
| 1 | JD-T1800 display | (adafruit or aliexpress for a ST7735R 160x128 display)
| 1 | N channel mosfet | PJC138K_R1_00001
| 1 | 22 uF 1206 capacitor | GRM319R61C226KE15D
| 1 | 1M ohm 603 resistor | AC0603FR-131ML
| 1 | 10k ohm 603 resistor | AC0603JR-0710KL

## Patch and Build Tangara FW

1. Clone and ensure you can build the official firmware repo or take a look at my fork 
(1/27/2025 the fork has the latest changes https://codeberg.org/NanoCodeBug/tangara-fw)

https://codeberg.org/cool-tech-zone/tangara-fw

2. Apply and build patch
```
git apply 0001-buttons.patch
```

The patch to inject a new driver class that handles the button front plate and forces it into button direction mode (in case you forgot to change it in settings before switching faceplates).

## Flash the Attiny826

I reccomend the Adafruit UPDI Friend, or building your own with a CH340 or arduino. 

I attached the assembled faceplate to my Tangara, plugged both the UPDI programmer and Tangara into the same computer, and connected only the UPDI pin to the UPDI programmer. 

My programmer is 3.3v, this may not be feasible if you have a 5 volt logic programmer. In that case I recommmend flashing the attiny before attaching the faceplate, the power and ground pins are not exposed as a header in this design, so attaching leads to the attiny power and ground will be necessary.

## 3d Print Faceplate and Buttons
Current deisgn requires a custom faceplate, but the area the buttons occupy is within the footprint of the existing toucwheel, so a insert that fits inside existing cases should be possible.

These were printed with 0.2 layer height out of PLA.

Todo: insert that is compatible with offical front plate